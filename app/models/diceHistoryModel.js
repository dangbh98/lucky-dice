// import library
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const diceHistorySchema = {
    _id: Schema.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        require: true
    },
    dice: {
        type: Number,
        require: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },

}

module.exports = mongoose.model("DiceHistory", diceHistorySchema);