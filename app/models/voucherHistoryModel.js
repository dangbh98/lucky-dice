// import library
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const voucherHistorySchema = {
    _id: Schema.Types.ObjectId,
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    voucher: {
        type: Schema.Types.ObjectId,
        ref: "Voucher",
        require: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },

}

module.exports = mongoose.model("VoucherHistory", voucherHistorySchema);