// import library
const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const prizeHistorySchema = {
    _id: Schema.Types.ObjectId,
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    prize: {
        type: Schema.Types.ObjectId,
        ref: "Prize",
        require: true
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
}

module.exports = mongoose.model("PrizeHistory", prizeHistorySchema);