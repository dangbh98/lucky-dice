// import library
const express = require("express");
const router = express.Router();

// import controller
const prizeController = require("../controllers/prizeController");
// import middleware
const prizeMiddleware = require("../middleware/prizeMiddleware");

router.post("/prizes", prizeMiddleware.createPrize, prizeController.createPrize);

router.get("/prizes", prizeMiddleware.getAllPrizes, prizeController.getAllPrizes);

router.get("/prizes/:prizeId", prizeMiddleware.getPrizeById, prizeController.getPrizeById);

router.put("/prizes/:prizeId", prizeMiddleware.updatePrizeById, prizeController.updatePrizeById);

router.delete("/prizes/:prizeId", prizeMiddleware.deletePrizeById, prizeController.deletePrizeById);

module.exports = router;
