// import library
const express = require("express");
const router = express.Router();

// import controller
const voucherHistoryController = require("../controllers/voucherHistoryController");
// import middleware
const voucherHistoryMiddleware = require("../middleware/voucherHistoryMiddleware");

router.post("/voucher-histories", voucherHistoryMiddleware.createVoucherHistory, voucherHistoryController.createVoucherHistory);

router.get("/voucher-histories", voucherHistoryMiddleware.getAllVoucherHistories, voucherHistoryController.getAllVoucherHistories);

router.get("/voucher-histories/:voucherHistoryId", voucherHistoryMiddleware.getVoucherHistoryById, voucherHistoryController.getVoucherHistoryById);

router.put("/voucher-histories/:voucherHistoryId", voucherHistoryMiddleware.updateVoucherHistoryById, voucherHistoryController.updateVoucherHistoryById);

router.delete("/voucher-histories/:voucherHistoryId", voucherHistoryMiddleware.deleteVoucherHistoryById, voucherHistoryController.deleteVoucherHistoryById);

module.exports = router;
