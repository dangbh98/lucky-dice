// import library
const express = require("express");
const router = express.Router();

// import controller
const voucherController = require("../controllers/voucherController");
// import middleware
const voucherMiddleware = require("../middleware/voucherMiddleware");

router.post("/vouchers", voucherMiddleware.createVoucher, voucherController.createVoucher);

router.get("/vouchers", voucherMiddleware.getAllVouchers, voucherController.getAllVouchers);

router.get("/vouchers/:voucherId", voucherMiddleware.getVoucherById, voucherController.getVoucherById);

router.put("/vouchers/:voucherId", voucherMiddleware.updateVoucherById, voucherController.updateVoucherById);

router.delete("/vouchers/:voucherId", voucherMiddleware.deleteVoucherById, voucherController.deleteVoucherById);

module.exports = router;
