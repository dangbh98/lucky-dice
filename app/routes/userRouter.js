// import library
const express = require("express");
const router = express.Router();

// import controller
const userController = require("../controllers/userController");
// import middleware
const userMiddleware = require("../middleware/userMiddleware");

router.post("/users", userMiddleware.createUser, userController.createUser);

router.get("/users", userMiddleware.getAllUsers, userController.getAllUsers);

router.get("/users/:userId", userMiddleware.getUserById, userController.getUserById);

router.put("/users/:userId", userMiddleware.updateUserById, userController.updateUserById);

router.delete("/users/:userId", userMiddleware.deleteUserById, userController.deleteUserById);

module.exports = router;
