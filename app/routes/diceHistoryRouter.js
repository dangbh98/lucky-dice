// import library
const express = require("express");
const router = express.Router();

// import controller
const diceHistoryController = require("../controllers/diceHistoryController");
// import middleware
const diceHistoryMiddleware = require("../middleware/diceHistoryMiddleware");

router.post("/dice-histories", diceHistoryMiddleware.createDiceHistory, diceHistoryController.createDiceHistory);

router.get("/dice-histories", diceHistoryMiddleware.getAllDiceHistory, diceHistoryController.getAllDiceHistories);

router.get("/dice-histories/:diceHistoryId", diceHistoryMiddleware.getDiceHistoryById, diceHistoryController.getDiceHistoryById);

router.put("/dice-histories/:diceHistoryId", diceHistoryMiddleware.updateDiceHistoryById, diceHistoryController.updateDiceHistoryById);

router.delete("/dice-histories/:diceHistoryId", diceHistoryMiddleware.deleteDiceHistoryById, diceHistoryController.deleteDiceHistoryById);

module.exports = router;
