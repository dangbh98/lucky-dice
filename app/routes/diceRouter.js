// import library
const express = require("express");
const router = express.Router();

// import controller
const diceController = require("../controllers/diceController");
// import middleware
// const diceMiddleware = require("../middleware/diceMiddleware");

router.post("/dices",  diceController.diceHandler);

module.exports = router;
