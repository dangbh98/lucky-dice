// import library
const express = require("express");
const router = express.Router();

// import controller
const prizeHistoryController = require("../controllers/prizeHistoryController");
// import middleware
const prizeHistoryMiddleware = require("../middleware/prizeHistoryMiddleware");

router.post("/prize-histories", prizeHistoryMiddleware.createPrizeHistory, prizeHistoryController.createPrizeHistory);

router.get("/prize-histories", prizeHistoryMiddleware.getAllPrizeHistories, prizeHistoryController.getAllPrizeHistories);

router.get("/prize-histories/:prizeHistoryId", prizeHistoryMiddleware.getPrizeHistoryById, prizeHistoryController.getPrizeHistoryById);

router.put("/prize-histories/:prizeHistoryId", prizeHistoryMiddleware.updatePrizeHistoryById, prizeHistoryController.updatePrizeHistoryById);

router.delete("/prize-histories/:prizeHistoryId", prizeHistoryMiddleware.deletePrizeHistoryById, prizeHistoryController.deletePrizeHistoryById);

module.exports = router;
