const createDiceHistory = (req, res, next) => {
    console.log(`Create diceHistory middleware`);
    next();
}

const getAllDiceHistory = (req, res, next) => {
    console.log(`get all diceHistory middleware`);
    next();
}

const getDiceHistoryById = (req, res, next) => {
    console.log(`Get diceHistory by id middleware`);
    next();
}

const updateDiceHistoryById = (req, res, next) => {
    console.log(`Update diceHistory by id middleware`);
    next();
}
const deleteDiceHistoryById = (req, res, next) => {
    console.log(`Delete diceHistory by id middleware`);
    next();
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById,
}