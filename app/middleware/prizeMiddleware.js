const createPrize = (req, res, next) => {
    console.log(`Create prize middleware`);
    next();
}

const getAllPrizes = (req, res, next) => {
    console.log(`get all prizes middleware`);
    next();
}

const getPrizeById = (req, res, next) => {
    console.log(`Get prize by id middleware`);
    next();
}

const updatePrizeById = (req, res, next) => {
    console.log(`Update prize by id middleware`);
    next();
}
const deletePrizeById = (req, res, next) => {
    console.log(`Delete prize by id middleware`);
    next();
}

module.exports = {
    createPrize,
    getAllPrizes,
    getPrizeById,
    updatePrizeById,
    deletePrizeById,
}