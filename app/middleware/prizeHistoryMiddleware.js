const createPrizeHistory = (req, res, next) => {
    console.log(`Create prizeHistory middleware`);
    next();
}

const getAllPrizeHistories = (req, res, next) => {
    console.log(`get all prizeHistories middleware`);
    next();
}

const getPrizeHistoryById = (req, res, next) => {
    console.log(`Get prizeHistory by id middleware`);
    next();
}

const updatePrizeHistoryById = (req, res, next) => {
    console.log(`Update prizeHistory by id middleware`);
    next();
}
const deletePrizeHistoryById = (req, res, next) => {
    console.log(`Delete prizeHistory by id middleware`);
    next();
}

module.exports = {
    createPrizeHistory,
    getAllPrizeHistories,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
}