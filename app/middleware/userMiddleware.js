const createUser = (req, res, next) => {
    console.log(`Create user middleware`);
    next();
}

const getAllUsers = (req, res, next) => {
    console.log(`get all users middleware`);
    next();
}

const getUserById = (req, res, next) => {
    console.log(`Get user by id middleware`);
    next();
}

const updateUserById = (req, res, next) => {
    console.log(`Update user by id middleware`);
    next();
}
const deleteUserById = (req, res, next) => {
    console.log(`Delete user by id middleware`);
    next();
}

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById,
}