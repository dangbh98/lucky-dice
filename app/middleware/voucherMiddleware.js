const createVoucher = (req, res, next) => {
    console.log(`Create voucher middleware`);
    next();
}

const getAllVouchers = (req, res, next) => {
    console.log(`get all vouchers middleware`);
    next();
}

const getVoucherById = (req, res, next) => {
    console.log(`Get voucher by id middleware`);
    next();
}

const updateVoucherById = (req, res, next) => {
    console.log(`Update voucher by id middleware`);
    next();
}
const deleteVoucherById = (req, res, next) => {
    console.log(`Delete voucher by id middleware`);
    next();
}

module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById,
}