const createVoucherHistory = (req, res, next) => {
    console.log(`Create voucherHistory middleware`);
    next();
}

const getAllVoucherHistories = (req, res, next) => {
    console.log(`get all voucherHistories middleware`);
    next();
}

const getVoucherHistoryById = (req, res, next) => {
    console.log(`Get voucherHistory by id middleware`);
    next();
}

const updateVoucherHistoryById = (req, res, next) => {
    console.log(`Update voucherHistory by id middleware`);
    next();
}
const deleteVoucherHistoryById = (req, res, next) => {
    console.log(`Delete voucherHistory by id middleware`);
    next();
}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistories,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById,
}