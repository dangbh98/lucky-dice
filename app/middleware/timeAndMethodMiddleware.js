const getCurrentTimeMiddleware = (req, res, next) => {
    console.log(`Time: ${new Date()}`);
    next();
}

const getRequestMethodMiddleware = (req, res, next) => {
    console.log(`Request method: ${req.method}`);
    next();
}

module.exports = {
    getCurrentTimeMiddleware,
    getRequestMethodMiddleware
}