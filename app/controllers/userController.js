// import model
const mongoose = require("mongoose");
const userModel = require("../models/userModel");

const createUser = (req, res) => {
    // 1. get data
    const {
        username,
        firstname,
        lastname,
        createdAt,
        updatedAt
    } = req.body;
    // 2. validate data
    if (username === undefined || username == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `username is not valid`
        })
    }
    if (firstname === undefined || firstname == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `firstname is not valid`
        })
    }
    if (lastname === undefined || lastname == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `lastname is not valid`
        })
    }
    // 3. process
    const createUserData = {
        _id: mongoose.Types.ObjectId(),
        username,
        firstname,
        lastname,
        createdAt,
        updatedAt
    }

    userModel.create(createUserData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        return res.status(201).json({
            status: `Create user successfully`,
            user: data
        })
    })
}

const getAllUsers = (req, res) => {
    // 1. get data
    // 2. validate data
    // 3. process
    userModel.find((err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        if (data.length == 0) {
            return res.status(404).json({
                status: `Not found`,
            })
        }
        return res.status(200).json({
            status: `get all users successfully`,
            users: data
        })
    })
}

const getUserById = (req, res) => {
    // 1. get data
    const vUserId = req.params.userId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `userId is not valid`
        })
    }
    // 3. process
    userModel.findById(vUserId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: `Not found`,
            })
        }
        return res.status(200).json({
            status: `Get user id: ${data._id} successfully`,
            user: data
        })
    })
}

const updateUserById = (req, res) => {
    // 1. get data
    const {
        username,
        firstname,
        lastname,
        createdAt,
        updatedAt
    } = req.body;
    const vUserId = req.params.userId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `userId is not valid`
        })
    }
    if (username == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `username is not valid`
        })
    }
    if (firstname == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `firstname is not valid`
        })
    }
    if (lastname == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `lastname is not valid`
        })
    }
    // 3. process
    const updateUserData = {}
    if (username) {
        updateUserData.username = username;
    }
    if (firstname) {
        updateUserData.firstname = firstname;
    }
    if (lastname) {
        updateUserData.lastname = lastname;
    }
    if (createdAt) {
        updateUserData.createdAt = createdAt;
    }
    if (updatedAt) {
        updateUserData.updatedAt = updatedAt;
    }else{
        updateUserData.updatedAt = Date.now();
    }

    userModel.findByIdAndUpdate(vUserId, updateUserData, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: `Not found`,
            })
        }
        return res.status(200).json({
            status: `Update user successfully`,
            user: data
        })
    })
}

const deleteUserById = (req, res) => {
    // 1. get data
    const vUserId = req.params.userId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `userId is not valid`
        })
    }
    // 3. process
    userModel.findByIdAndDelete(vUserId, (err, data) => {
        if (err) {
            return res.status(500).json({
                status: `Internal server error`,
                message: err.message
            })
        }
        if (!data) {
            return res.status(404).json({
                status: `Not found`,
            })
        }
        return res.status(204).json();
    })
}

module.exports = {
    createUser,
    getAllUsers,
    getUserById,
    updateUserById,
    deleteUserById
}