// import model
const mongoose = require("mongoose");
const voucherModel = require("../models/voucherModel");

const createVoucher = (req, res) => {
    // 1. get data
    const {
        code,
        discount,
        note,
        createdAt,
        updatedAt,
    } = req.body;
    // 2. validate data
    if (code === undefined || code == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `code is not valid`
        })
    }
    if (!(Number.isInteger(discount) && discount >= 0)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `discount is not valid`
        })
    }
    // 3. process
    const createVoucherData = {
        _id: mongoose.Types.ObjectId(),
        code,
        discount,
        note,
        createdAt,
        updatedAt,
    }

    voucherModel.create(createVoucherData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        return res.status(201).json({
            status: `Create voucher successfully`,
            voucher: data
        })
    })
}

const getAllVouchers = (req, res) => {
    // 1. get data
    // 2. validate data
    // 3. process
    voucherModel.find((err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (data.length == 0) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `get all vouchers successfully`,
            vouchers: data
        })
    })
}

const getVoucherById = (req, res) => {
    // 1. get data
    const vVoucherId = req.params.voucherId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucherId is not valid`
        })
    }
    // 3. process
    voucherModel.findById(vVoucherId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Get voucher id: ${data._id} successfully`,
            voucher: data
        })
    })
}

const updateVoucherById = (req, res) => {
    // 1. get data
    const {
        code,
        discount,
        note,
        createdAt,
        updatedAt,
    } = req.body;
    const vVoucherId = req.params.voucherId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucherId is not valid`
        })
    }
    if (code == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `code is not valid`
        })
    }
    if (discount !== undefined && !(Number.isInteger(discount) && discount >= 0)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `discount is not valid`
        })
    }
    // 3. process
    const updateVoucherData = {}
    if (code) {
        updateVoucherData.code = code;
    }
    if (discount) {
        updateVoucherData.discount = discount;
    }
    if (note) {
        updateVoucherData.note = note;
    }
    if (createdAt) {
        updateVoucherData.createdAt = createdAt;
    }
    if (updatedAt) {
        updateVoucherData.updatedAt = updatedAt;
    } else {
        updateVoucherData.updatedAt = Date.now();
    }

    voucherModel.findByIdAndUpdate(vVoucherId, updateVoucherData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Update voucher id: ${data._id} successfully`,
            voucher: data
        })
    })
}

const deleteVoucherById = (req, res) => {
    // 1. get data
    const vVoucherId = req.params.voucherId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucherId is not valid`
        })
    }
    // 3. process
    voucherModel.findByIdAndDelete(vVoucherId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(204).json();
    })
}

const handleError = (paramError, paramResponse) => {
    return paramResponse.status(500).json({
        status: `Internal server error`,
        message: paramError.message
    })
}

const handleNotFound = (paramResponse) => {
    return paramResponse.status(404).json({
        status: "Not found"
    })
}

module.exports = {
    createVoucher,
    getAllVouchers,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}