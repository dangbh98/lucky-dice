// import model
const mongoose = require("mongoose");
const prizeHistoryModel = require("../models/prizeHistoryModel");
const userModel = require("../models/userModel");

const createPrizeHistory = (req, res) => {
    // 1. get data
    const {
        user,
        prize,
        createdAt,
        updatedAt,
    } = req.body;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `user is not valid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prize is not valid`
        })
    }
    // 3. process
    const createPrizeHistoryData = {
        _id: mongoose.Types.ObjectId(),
        user,
        prize,
        createdAt,
        updatedAt,
    }

    prizeHistoryModel.create(createPrizeHistoryData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        return res.status(201).json({
            status: `Create prizeHistory successfully`,
            prizeHistory: data
        })
    })
}

const getAllPrizeHistories = (req, res) => {
    const vUserId = req.query.userId;
    const vUsername = req.query.username;
    // 2. validate data
    if (vUserId !== undefined && !mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: "Bad request",
            message: `userId is not valid`
        })
    }
    if (vUsername !== undefined && !vUsername) {
        return res.status(400).json({
            status: "Bad request",
            message: `username is not valid`
        })
    }
    // 3. process
    if (vUsername) {
        userModel.findOne({ username: vUsername }, (findUserErr, foundUser) => {
            if (findUserErr) {
                return handleError(findUserErr, res);
            }else if (!foundUser) {
                return res.status(404).json({
                    status: `user ${vUsername} is not exist`,
                    prizeHistories: []
                })
            }

            prizeHistoryModel.find({user: foundUser._id})
            .populate("prize")
            .exec((prizeHistoriesErr, prizeHistoryData) => {
                if (prizeHistoriesErr) {
                    return handleError(prizeHistoriesErr, res);
                }
                if (prizeHistoryData.length == 0) {
                    return handleNotFound(res);
                }
                return res.status(200).json({
                    status: `get all prizeHistories of username ${vUsername} successfully`,
                    prizeHistories: prizeHistoryData
                })
            })
        })
    } else {
        let condition = {}
        if (vUserId) {
            condition.user = vUserId
        }
        prizeHistoryModel.find(condition)
        .populate("prize")
        .exec((prizeHistoriesErr, prizeHistoryData) => {
            if (prizeHistoriesErr) {
                return handleError(prizeHistoriesErr, res);
            }
            if (prizeHistoryData.length == 0) {
                return handleNotFound(res);
            }
            if (vUserId) {
                return res.status(200).json({
                    status: `get all prizeHistories of user id ${vUserId} successfully`,
                    prizeHistories: prizeHistoryData
                })
            }
            return res.status(200).json({
                status: `get all prizeHistories successfully`,
                prizeHistories: prizeHistoryData
            })
        })
    }

}

const getPrizeHistoryById = (req, res) => {
    // 1. get data
    const vPrizeHistoryId = req.params.prizeHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vPrizeHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizeHistoryId is not valid`
        })
    }
    // 3. process
    prizeHistoryModel.findById(vPrizeHistoryId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Get prizeHistory id: ${data._id} successfully`,
            prizeHistory: data
        })
    })
}

const updatePrizeHistoryById = (req, res) => {
    // 1. get data
    const {
        user,
        prize,
        createdAt,
        updatedAt,
    } = req.body;
    const vPrizeHistoryId = req.params.prizeHistoryId;
    // 2. validate data
    if (user !== undefined && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `user is not valid`
        })
    }
    if (user !== undefined && !mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prize is not valid`
        })
    }
    // 3. process
    const updatePrizeHistoryData = {}
    if (user) {
        updatePrizeHistoryData.user = user;
    }
    if (prize) {
        updatePrizeHistoryData.prize = prize;
    }
    if (createdAt) {
        updatePrizeHistoryData.createdAt = createdAt;
    }
    if (updatedAt) {
        updatePrizeHistoryData.updatedAt = updatedAt;
    } else {
        updatePrizeHistoryData.updatedAt = Date.now();
    }

    prizeHistoryModel.findByIdAndUpdate(vPrizeHistoryId, updatePrizeHistoryData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Update prizeHistory id: ${data._id} successfully`,
            prizeHistory: data
        })
    })
}

const deletePrizeHistoryById = (req, res) => {
    // 1. get data
    const vPrizeHistoryId = req.params.prizeHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vPrizeHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizeHistoryId is not valid`
        })
    }
    // 3. process
    prizeHistoryModel.findByIdAndDelete(vPrizeHistoryId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(204).json();
    })
}

const handleError = (paramError, paramResponse) => {
    return paramResponse.status(500).json({
        status: `Internal server error`,
        message: paramError.message
    })
}

const handleNotFound = (paramResponse) => {
    return paramResponse.status(404).json({
        status: "Not found"
    })
}

module.exports = {
    createPrizeHistory,
    getAllPrizeHistories,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}