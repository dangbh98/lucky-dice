// import model
const mongoose = require("mongoose");
const diceHistoryModel = require("../models/diceHistoryModel");
const userModel = require("../models/userModel");

const createDiceHistory = (req, res) => {
    // 1. get data
    const {
        user,
        createdAt,
        updatedAt
    } = req.body;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `user is not valid`
        })
    }
    // 3. process
    var randomNumber = Math.floor(Math.random() * 6 + 1)

    const createDiceHistoryData = {
        _id: mongoose.Types.ObjectId(),
        user,
        dice: randomNumber,
        createdAt,
        updatedAt
    }

    userModel.findById(user, (err1, data1) => {
        if (err1) {
            return handleError(err1, res);
        }
        if (!data1) {
            return handleNotFound(res);
        }
        diceHistoryModel.create(createDiceHistoryData, (err, data) => {
            if (err) {
                return handleError(err, res);
            }
            return res.status(201).json({
                status: `Create diceHistory successfully`,
                diceHistory: data
            })
        })
    })
}

const getAllDiceHistories = (req, res) => {
    // 1. get data
    const vUserId = req.query.userId;
    const vUsername = req.query.username;
    console.log(vUsername);
    // 2. validate data
    if (vUserId !== undefined && !mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: "Bad request",
            message: `userId is not valid`
        })
    }
    if (vUsername !== undefined && !vUsername) {
        return res.status(400).json({
            status: "Bad request",
            message: `username is not valid`
        })
    }
    // 3. process
    if (vUsername) {
        userModel.findOne({ username: vUsername }, (findUserErr, foundUser) => {
            if (findUserErr) {
                return handleError(findUserErr, res);
            } else if (!foundUser) {
                return res.status(404).json({
                    status: `user ${vUsername} is not exist`,
                    diceHistories: []
                })
            } else {
                diceHistoryModel.find({ user: foundUser._id }, (diceHistoriesErr, diceHistoriesData) => {
                    if (diceHistoriesErr) {
                        return handleError(diceHistoriesErr, res);
                    }
                    if (!diceHistoriesData) {
                        return handleNotFound(res);
                    }
                    return res.status(200).json({
                        status: `get all diceHistories of user ${vUsername} successfully`,
                        diceHistories: diceHistoriesData
                    })
                })
            }
        });

    } else {
        const condition = {};
        if (vUserId) {
            condition.user = vUserId;
        }
        diceHistoryModel.find(condition, (err, data) => {
            if (err) {
                return handleError(err, res);
            }
            if (data.length == 0) {
                return handleNotFound(res);
            }
            if (vUserId) {
                return res.status(200).json({
                    status: `get all diceHistories of user id ${vUserId} successfully`,
                    diceHistories: data
                })
            }
            return res.status(200).json({
                status: `get all diceHistories successfully`,
                diceHistories: data
            })
        })

    }
}



const getDiceHistoryById = (req, res) => {
    // 1. get data
    const vDiceHistoryId = req.params.diceHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vDiceHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `diceHistoryId is not valid`
        })
    }
    // 3. process
    diceHistoryModel.findById(vDiceHistoryId)
        .populate("user")
        .exec((err, data) => {
            if (err) {
                return handleError(err, res);
            }
            if (!data) {
                return handleNotFound(res);
            }
            return res.status(200).json({
                status: `Get diceHistory id: ${data._id} successfully`,
                diceHistory: data
            })
        })
}

const updateDiceHistoryById = (req, res) => {
    // 1. get data
    const {
        user,
        createdAt,
        updatedAt
    } = req.body;
    const vDiceHistoryId = req.params.diceHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vDiceHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `diceHistoryId is not valid`
        })
    }
    if (user !== undefined && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `user is not valid`
        })
    }
    // 3. process
    const updateDiceHistoryData = {}
    if (user) {
        updateDiceHistoryData.user = user;
    }
    if (createdAt) {
        updateDiceHistoryData.createdAt = createdAt;
    }
    if (updatedAt) {
        updateDiceHistoryData.updatedAt = updatedAt;
    } else {
        updateDiceHistoryData.updatedAt = Date.now();
    }

    diceHistoryModel.findByIdAndUpdate(vDiceHistoryId, updateDiceHistoryData)
        .exec((err, data) => {
            if (err) {
                return handleError(err, res);
            }
            if (!data) {
                return handleNotFound(res);
            }
            return res.status(200).json({
                status: `Update diceHistory successfully`,
                diceHistory: data
            })
        })
}

const deleteDiceHistoryById = (req, res) => {
    // 1. get data
    const vDiceHistoryId = req.params.diceHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vDiceHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `diceHistoryId is not valid`
        })
    }
    // 3. process
    diceHistoryModel.findByIdAndDelete(vDiceHistoryId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(204).json();
    })
}

const handleError = (paramError, paramResponse) => {
    return paramResponse.status(500).json({
        status: `Internal server error`,
        message: paramError.message
    })
}

const handleNotFound = (paramResponse) => {
    return paramResponse.status(404).json({
        status: "Not found"
    })
}
module.exports = {
    createDiceHistory,
    getAllDiceHistories,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById
}