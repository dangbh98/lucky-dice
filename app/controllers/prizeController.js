// import model
const mongoose = require("mongoose");
const prizeModel = require("../models/prizeModel");

const createPrize = (req, res) => {
    // 1. get data
    const {
        name,
        description,
        createdAt,
        updatedAt,
    } = req.body;
    // 2. validate data
    if (name === undefined || name == "") {
        return res.status(400).json({
            status: `Bad request`,
            message: `name is not valid`
        })
    }
    // 3. process
    const createPrizeData = {
        _id: mongoose.Types.ObjectId(),
        name,
        description,
        createdAt,
        updatedAt,
    }

    prizeModel.create(createPrizeData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        return res.status(201).json({
            status: `Create prize successfully`,
            prize: data
        })
    })
}

const getAllPrizes = (req, res) => {
    // 1. get data
    // 2. validate data
    // 3. process
    prizeModel.find((err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (data.length == 0) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `get all prizes successfully`,
            prizes: data
        })
    })
}

const getPrizeById = (req, res) => {
    // 1. get data
    const vPrizeId = req.params.prizeId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vPrizeId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizeId is not valid`
        })
    }
    // 3. process
    prizeModel.findById(vPrizeId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Get prize id: ${data._id} successfully`,
            prize: data
        })
    })
}

const updatePrizeById = (req, res) => {
    // 1. get data
    const {
        name,
        description,
        createdAt,
        updatedAt
    } = req.body;
    const vPrizeId = req.params.prizeId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vPrizeId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizeId is not valid`
        })
    }
    if (name !== undefined && name == "") {
        return res.statua(400).json({
            status: `Bad request`,
            message: `name is not valid`
        })
    }
    // 3. process
    const updatePrizeData = {}
    if (name) {
        updatePrizeData.name = name;
    }
    if (description) {
        updatePrizeData.description = description;
    }
    if (createdAt) {
        updatePrizeData.createdAt = createdAt;
    }
    if (updatedAt) {
        updatePrizeData.updatedAt = updatedAt;
    } else {
        updatePrizeData.updatedAt = Date.now();
    }

    prizeModel.findByIdAndUpdate(vPrizeId, updatePrizeData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Update prize id: ${data._id} successfully`,
            prize: data
        })
    })
}

const deletePrizeById = (req, res) => {
    // 1. get data
    const vPrizeId = req.params.prizeId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vPrizeId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `prizeId is not valid`
        })
    }
    // 3. process
    prizeModel.findByIdAndDelete(vPrizeId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(204).json();
    })
}

const handleError = (paramError, paramResponse) => {
    return paramResponse.status(500).json({
        status: `Internal server error`,
        message: paramError.message
    })
}

const handleNotFound = (paramResponse) => {
    return paramResponse.status(404).json({
        status: "Not found"
    })
}

module.exports = {
    createPrize,
    getAllPrizes,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}