// import model
const mongoose = require("mongoose");
const voucherHistoryModel = require("../models/voucherHistoryModel");
const userModel = require("../models/userModel");

const createVoucherHistory = (req, res) => {
    const {
        user,
        voucher,
        createdAt,
        updatedAt,
    } = req.body;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `user is not valid`
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher is not valid`
        })
    }
    // 3. process
    const createVoucherHistoryData = {
        _id: mongoose.Types.ObjectId(),
        user,
        voucher,
        createdAt,
        updatedAt,
    }

    voucherHistoryModel.create(createVoucherHistoryData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        return res.status(201).json({
            status: `Create voucherHistory successfully`,
            voucherHistory: data
        })
    })
}

const getAllVoucherHistories = (req, res) => {
    const vUserId = req.query.userId;
    const vUsername = req.query.username;
    // 2. validate data
    if (vUserId !== undefined && !mongoose.Types.ObjectId.isValid(vUserId)) {
        return res.status(400).json({
            status: "Bad request",
            message: `userId is not valid`
        })
    }
    if (vUsername !== undefined && !vUsername) {
        return res.status(400).json({
            status: "Bad request",
            message: `username is not valid`
        })
    }
    // 3. process
    if (vUsername) {
        userModel.findOne({ username: vUsername }, (findUserErr, foundUser) => {
            if (findUserErr) {
                return handleError(findUserErr, res);
            } else if (!foundUser) {
                return res.status(404).json({
                    status: `user ${vUsername} is not exist`,
                    voucherHistories: []
                })
            } else {
                voucherHistoryModel.find({ user: foundUser._id })
                    .populate("voucher")
                    .exec(
                        (voucherHistoryErr, voucherHistoryData) => {
                            if (voucherHistoryErr) {
                                return handleError(voucherHistoryErr, res);
                            }
                            if (!voucherHistoryData) {
                                return handleNotFound(res);
                            }
                            return res.status(200).json({
                                status: `get all voucherHistories of user ${vUsername} successfully`,
                                voucherHistories: voucherHistoryData
                            })
                        })
            }
        });
    } else {
        let condition = {}
        if (vUserId) {
            condition.user = vUserId;
        }
        voucherHistoryModel.find(condition, (err, data) => {
            if (err) {
                return handleError(err, res);
            }
            if (data.length == 0) {
                return handleNotFound(res);
            }
            if (vUserId) {
                return res.status(200).json({
                    status: `get all voucherHistories of user id ${vUserId} successfully`,
                    voucherHistories: data
                })
            }
            return res.status(200).json({
                status: `get all voucherHistories successfully`,
                voucherHistories: data
            })
        })
    }
}

const getVoucherHistoryById = (req, res) => {
    // 1. get data
    const vVoucherHistoryId = req.params.voucherHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucherHistoryId is not valid`
        })
    }
    // 3. process
    voucherHistoryModel.findById(vVoucherHistoryId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Get voucherHistory id: ${data._id} successfully`,
            voucherHistory: data
        })
    })
}

const updateVoucherHistoryById = (req, res) => {
    // 1. get data
    const {
        user,
        voucher,
        createdAt,
        updatedAt,
    } = req.body;
    const vVoucherHistoryId = req.params.voucherHistoryId;
    // 2. validate data
    if (user !== undefined && !mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `user is not valid`
        })
    }
    if (voucher !== undefined && !mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher is not valid`
        })
    }
    // 3. process
    const updateVoucherHistoryData = req.body;

    if (updatedAt) {
        updateVoucherHistoryData.updatedAt = updatedAt;
    } else {
        updateVoucherHistoryData.updatedAt = Date.now();
    }

    voucherHistoryModel.findByIdAndUpdate(vVoucherHistoryId, updateVoucherHistoryData, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(200).json({
            status: `Update voucherHistory id: ${data._id} successfully`,
            voucherHistory: data
        })
    })
}

const deleteVoucherHistoryById = (req, res) => {
    // 1. get data
    const vVoucherHistoryId = req.params.voucherHistoryId;
    // 2. validate data
    if (!mongoose.Types.ObjectId.isValid(vVoucherHistoryId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucherHistoryId is not valid`
        })
    }
    // 3. process
    voucherHistoryModel.findByIdAndDelete(vVoucherHistoryId, (err, data) => {
        if (err) {
            return handleError(err, res);
        }
        if (!data) {
            return handleNotFound(res);
        }
        return res.status(204).json();
    })
}

const handleError = (paramError, paramResponse) => {
    return paramResponse.status(500).json({
        status: `Internal server error`,
        message: paramError.message
    })
}

const handleNotFound = (paramResponse) => {
    return paramResponse.status(404).json({
        status: "Not found"
    })
}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistories,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}