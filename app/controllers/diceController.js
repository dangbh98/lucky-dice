const mongoose = require("mongoose");

const diceHistoryModel = require("../models/diceHistoryModel");
const prizeHistoryModel = require("../models/prizeHistoryModel");
const prizeModel = require("../models/prizeModel");
const userModel = require("../models/userModel");
const voucherHistoryModel = require("../models/voucherHistoryModel");
const voucherModel = require("../models/voucherModel");

const diceHandler = (req, res) => {
    // B1: Chuẩn bị dữ liệu
    const {
        username,
        firstname,
        lastname,
    } = req.body

    // Random 1 giá trị xúc xắc bất kỳ
    let dice = Math.floor(Math.random() * 6 + 1);

    // B2: Validate dữ liệu từ request body
    if (!username) {
        return res.status(400).json({
            status: "Bad request",
            message: "Username is not valid"
        })
    }

    if (!firstname) {
        return res.status(400).json({
            status: "Bad request",
            message: "Firstname is not valid"
        })
    }

    if (!lastname) {
        return res.status(400).json({
            status: "Bad request",
            message: "Lastname is not valid"
        })
    }

    // Sử dụng userModel tìm kiếm bằng username
    userModel.findOne({ username }, (errorFindUser, userExist) => {
        if (errorFindUser) {
            return handleError(errorFindUser, res)
        } else if (!userExist) {
            // Nếu user không tồn tại trong hệ thống
            // Tạo user mới
            const userData = {
                _id: mongoose.Types.ObjectId(),
                username,
                firstname,
                lastname
            }
            userModel.create(userData, (errCreateUser, userCreated) => {
                if (errCreateUser) {
                    return handleError(errCreateUser, res);
                } else {
                    // Xúc xắc 1 lần, lưu lịch sử vào Dice History
                    const diceHistoryData = {
                        _id: mongoose.Types.ObjectId(),
                        user: userCreated._id,
                        dice: dice
                    }
                    diceHistoryModel.create(diceHistoryData, (errorDiceHistoryCreate, diceHistoryCreated) => {
                        if (errorDiceHistoryCreate) {
                            return handleError(errorDiceHistoryCreate, res);
                        } else if (dice < 3) {
                            // Nếu dice < 3, không nhận được voucher và prize gì cả
                            return res.status(200).json({
                                dice: dice,
                                prize: null,
                                voucher: null
                            })
                        } else {
                            // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                            voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                                let random = Math.floor(Math.random() * countVoucher);

                                voucherModel.findOne().skip(random).exec((errorFindVoucher, randomVoucher) => {
                                    // Lưu voucher History
                                    const voucherHistoryData = {
                                        _id: mongoose.Types.ObjectId(),
                                        user: userCreated._id,
                                        voucher: randomVoucher._id
                                    }
                                    voucherHistoryModel.create(voucherHistoryData, (errorCreateVoucherHistory, voucherHistoryCreated) => {
                                        if (errorCreateVoucherHistory) {
                                            return handleError(errorCreateVoucherHistory, res);
                                        } else {
                                            // User mới không có prize
                                            return res.status(200).json({
                                                dice: dice,
                                                prize: null,
                                                voucher: randomVoucher
                                            })
                                        }
                                    })
                                })
                            })
                        }
                    })
                }
            })
        } else {
            // Nếu user đã tồn tại trong hệ thống
            // Xúc xắc 1 lần, lưu lịch sử vào Dice History
            diceHistoryModel.create({
                _id: mongoose.Types.ObjectId(),
                user: userExist._id,
                dice: dice
            }, (errorDiceHistoryCreate, diceHistoryCreated) => {
                if (errorDiceHistoryCreate) {
                    return handleError(errorDiceHistoryCreate, res);
                } else if (dice < 3) {
                    // Nếu dice < 3, không nhận được voucher và prize gì cả
                    return res.status(200).json({
                        dice: dice,
                        prize: null,
                        voucher: null
                    })
                } else {
                    // Nếu dice > 3, thực hiện lấy random một giá trị voucher bất kỳ trong hệ thống
                    voucherModel.count().exec((errorCountVoucher, countVoucher) => {
                        let random =  Math.floor(Math.random() * countVoucher);
                        console.log(random);
                        voucherModel.findOne().skip(random).exec((errorFindVoucher, randomVoucher) => {
                            // Lưu voucher History
                            voucherHistoryModel.create({
                                _id: mongoose.Types.ObjectId(),
                                user: userExist._id,
                                voucher: randomVoucher._id
                            }, (errorCreateVoucherHistory, voucherHistoryCreated) => {
                                if (errorCreateVoucherHistory) {
                                    return handleError(errorCreateVoucherHistory, res);
                                } else {
                                    // Lấy 3 lần gieo xúc xắc gần nhất của user
                                    diceHistoryModel.find()
                                        .sort({
                                            _id: "desc"
                                        })
                                        .limit(3)
                                        .exec((errorFindLast3DiceHistory, last3DiceHistory) => {
                                            if (errorFindLast3DiceHistory) {
                                                return handleError(errorFindLast3DiceHistory, res);
                                            } else if (last3DiceHistory.length < 3) {
                                                // Nếu chưa ném đủ 3 lần, không nhận được prize
                                                return res.status(200).json({
                                                    dice: dice,
                                                    prize: null,
                                                    voucher: randomVoucher
                                                })
                                            } else {
                                                // console.log(last3DiceHistory)
                                                // Kiểm tra 3 dice gần nhất
                                                let checkHavePrize = true;
                                                last3DiceHistory.forEach(diceHistory => {
                                                    if (diceHistory.dice < 3) {
                                                        // Nếu 3 lần gần nhất có 1 lần xúc xắc nhỏ hơn 3 => không nhận được giải thưởng
                                                        checkHavePrize = false;
                                                    }
                                                });

                                                if (!checkHavePrize) {
                                                    return res.status(200).json({
                                                        dice: dice,
                                                        prize: null,
                                                        voucher: randomVoucher
                                                    })
                                                } else {
                                                    // Nếu đủ điều kiện nhận giải thưởng, tiến hành lấy random 1 prize trong prize Model
                                                    prizeModel.count().exec((errorCountPrize, countPrize) => {
                                                        let random = Math.floor(Math.random() * countPrize);
                                                        prizeModel.findOne().skip(random).exec((errorFindPrize, randomPrize) => {
                                                            // Lưu prize History
                                                            prizeHistoryModel.create({
                                                                _id: mongoose.Types.ObjectId(),
                                                                user: userExist._id,
                                                                prize: randomPrize._id
                                                            }, (errorCreatePrizeHistory, voucherPrizeCreated) => {
                                                                if (errorCreatePrizeHistory) {
                                                                    return res.status(500).json({
                                                                        status: "Error 500: Internal server error",
                                                                        message: errorCreatePrizeHistory.message
                                                                    })
                                                                } else {
                                                                    // Trả về kết quả cuối cùng
                                                                    return res.status(200).json({
                                                                        dice: dice,
                                                                        prize: randomPrize,
                                                                        voucher: randomVoucher
                                                                    })
                                                                }
                                                            })
                                                        })
                                                    })
                                                }
                                            }
                                        })
                                }
                            })
                        })
                    })
                }
            })
        }
    })
}


const handleError = (err, res) => {
    return res.status(500).json({
        status: "Error 500: Internal server error",
        message: errorFindUser.message
    })
}

const handleNotFound = (res) => {
    return res.status(500).json({
        status: "Not Found",
    })
}
module.exports = {
    diceHandler
}