// import libraries
const express = require("express");
const path = require("path");
const mongoose = require("mongoose");

// import middlewares
const timeAndMethodMiddleware = require("./app/middleware/timeAndMethodMiddleware")

//import routers
const userRouter = require("./app/routes/userRouter");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");
const diceRouter = require("./app/routes/diceRouter");

// declare express app
const app = express();

// declare port to running app
const port = 8000;

// connect to mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Lucky_Dice",(err) => {
    if (err) throw err;
    console.log(`Connected to mongoDB`);
})

//config to read body json
app.use(express.json());

app.get("/random-number", timeAndMethodMiddleware.getCurrentTimeMiddleware, timeAndMethodMiddleware.getRequestMethodMiddleware,(req, res) => {
    var randomNumber = Math.floor(Math.random() * 6 + 1)
    return res.status(200).json({
        randomNumber: randomNumber
    })
})

// config static path
app.use(express.static(__dirname + "/views"));

// home api
app.get("/", timeAndMethodMiddleware.getCurrentTimeMiddleware, timeAndMethodMiddleware.getRequestMethodMiddleware, (req, res) => {
    res.sendFile(path.join(__dirname + "/views/index.html"))
})

// declare user router
app.use("/devcamp-lucky-dice", userRouter);
// declare diceHistory router
app.use("/devcamp-lucky-dice", diceHistoryRouter);
// declare prize router
app.use("/devcamp-lucky-dice", prizeRouter);
// declare voucher router
app.use("/devcamp-lucky-dice", voucherRouter);
// declare prizeHistory router
app.use("/devcamp-lucky-dice", prizeHistoryRouter);
// declare voucherHistory router
app.use("/devcamp-lucky-dice", voucherHistoryRouter);
// declare voucherHistory router
app.use("/devcamp-lucky-dice", diceRouter);
// running app in port

app.listen(port, () => {
    console.log(`App is listening on port ${port}`);
})